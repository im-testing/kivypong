# KivyPong

[![Maintainability](https://api.codeclimate.com/v1/badges/f2a312e4f28056977123/maintainability)](https://codeclimate.com/github/ryan-ccn/kivypong/maintainability)

KivyPong is a Pong application game based on the Python library [Kivy](https://kivy.org/#home). It is fun to play, but you have to install some prerequisites before you play.

### Prerequisites

 1. Install Kivy on your computer. See [this guide](https://kivy.org/doc/stable/installation/installation.html)

 2. Run the app by typing this in your terminal:
    ```bash
    $ kivy main.py
    ```
    Or simply dragging the main.py file onto the Kivy application will work too.

Enjoy!